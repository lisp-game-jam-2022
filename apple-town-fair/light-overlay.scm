;;; Copyright © 2022 David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.
(define-module (apple-town-fair light-overlay)
  #:use-module (apple-town-fair common)
  #:use-module (apple-town-fair config)
  #:use-module (catbird node)
  #:use-module (catbird node-2d)
  #:use-module (chickadee graphics color)
  #:use-module (oop goops)
  #:export (<light-overlay>))

;; A simple translucent color to overlay on the scene to poorly but
;; adequately simulate the time of day.
(define-class <light-overlay> (<node-2d>)
  (time #:accessor time #:init-keyword #:time #:init-value 6 #:observe? #t))

(define-method (default-width (light <light-overlay>))
  %game-width)

(define-method (default-height (light <light-overlay>))
  %game-height)

(define-method (on-boot (light <light-overlay>))
  (attach-to light
             (make <canvas>
               #:name 'overlay))
  (refresh light))

(define-method (on-change (light <light-overlay>) slot old new)
  (refresh light))

(define-method (overlay-color (light <light-overlay>))
  (let ((t (time light)))
    (cond
     ((= t 6)
      (make-color 0.0 0.0 0.0 0.5))
     ((= t 7)
      (make-color 0.0 0.0 0.0 0.3))
     ((= t 8)
      (make-color 0.0 0.0 0.0 0.1))
     ((= t 9)
      (make-color 0.0 0.0 0.0 0.0))
     ((= t 10)
      (make-color 0.0 0.0 0.0 0.0))
     ((= t 11)
      (make-color 0.0 0.0 0.0 0.0))
     ((= t 12)
      (make-color 0.0 0.0 0.0 0.0))
     ((= t 13)
      (make-color 0.0 0.0 0.0 0.0))
     ((= t 14)
      (make-color 0.0 0.0 0.0 0.0))
     ((= t 15)
      (make-color 0.0 0.0 0.0 0.0))
     ((= t 16)
      (make-color 0.0 0.0 0.0 0.0))
     ((= t 17)
      (make-color 0.0 0.0 0.0 0.1))
     ((= t 18)
      (make-color 0.0 0.0 0.0 0.2))
     ((= t 19)
      (make-color 0.0 0.0 0.0 0.4))
     ((= t 20)
      (make-color 0.0 0.0 0.0 0.7))
     ((= t 21)
      (make-color 0.0 0.0 0.0 0.8))
     (else
      (make-color 0.0 0.0 0.0 0.0)))))

(define-method (refresh (light <light-overlay>))
  (set! (painter (& light overlay))
        (full-screen-rectangle (overlay-color light))))
