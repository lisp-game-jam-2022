;;; Copyright © 2022 David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.
(define-module (apple-town-fair assets)
  #:use-module (catbird asset)
  #:export (monogram-font
            chickadee-image
            dialog-box-image
            home-background-image
            common-background-image
            trail-background-image
            farm-stand-background-image
            orchard-background-image
            burned-town-image))

(define (scope-datadir file-name)
  (let ((prefix (or (getenv "APPLE_TOWN_FAIR_DATADIR") (getcwd))))
    (string-append prefix "/" file-name)))

(define (font-file file-name)
  (scope-datadir (string-append "assets/fonts/" file-name)))

(define (image-file file-name)
  (scope-datadir (string-append "assets/images/" file-name)))

(define (audio-file file-name)
  (scope-datadir (string-append "assets/audio/" file-name)))

(define-font monogram-font (font-file "monogram_extended.ttf") 12)
(define-image chickadee-image (image-file "chickadee.png"))
(define-image dialog-box-image (image-file "dialog-box.png"))
(define-image home-background-image (image-file "home.png"))
(define-image common-background-image (image-file "common.png"))
(define-image trail-background-image (image-file "trail.png"))
(define-image farm-stand-background-image (image-file "farm-stand.png"))
(define-image orchard-background-image (image-file "orchard.png"))
(define-image burned-town-image (image-file "burned-town.png"))
