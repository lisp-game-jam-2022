;;; Copyright © 2022 David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.
(define-module (apple-town-fair place)
  #:use-module (catbird config)
  #:use-module (catbird node)
  #:use-module (catbird node-2d)
  #:use-module (catbird scene)
  #:use-module (oop goops)
  #:export (<action>
            condition
            duration
            exec
            performable?
            perform
            <place>
            title
            actions))

(define-root-class <action> ()
  (name #:accessor name #:init-keyword #:name)
  (condition #:accessor condition #:init-keyword #:condition
             #:init-form (const #t))
  (exec #:accessor exec #:init-keyword #:exec))

(define-method (performable? (action <action>))
  ((condition action) (current-scene)))

(define-method (perform (action <action>))
  ((exec action) (current-scene)))

(define-class <place> (<node-2d>)
  (title #:accessor title #:init-keyword #:title #:init-value "Unknown")
  (background #:accessor background #:init-keyword #:background)
  (actions #:accessor actions #:init-keyword #:actions #:init-value '()))

(define-method (on-boot (place <place>))
  (attach-to place
             (make <sprite>
               #:texture (slot-ref place 'background))))
