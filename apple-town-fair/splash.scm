;;; Copyright © 2022 David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.
(define-module (apple-town-fair splash)
  #:use-module (apple-town-fair assets)
  #:use-module (apple-town-fair config)
  #:use-module (apple-town-fair common)
  #:use-module (apple-town-fair game)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee graphics path)
  #:use-module (chickadee math vector)
  #:use-module (chickadee scripting)
  #:use-module (oop goops)
  #:use-module (catbird)
  #:use-module (catbird camera)
  #:use-module (catbird kernel)
  #:use-module (catbird mode)
  #:use-module (catbird node)
  #:use-module (catbird node-2d)
  #:use-module (catbird region)
  #:use-module (catbird scene)
  #:export (launch-game))

(define-class <splash-mode> (<major-mode>))

(define %text-color (rgb #x181425))
(define %background-color (rgb #xead4aa))

(define-method (on-enter (mode <splash-mode>))
  (let ((scene (parent mode)))
    (attach-to scene
               (make <canvas>
                 #:painter
                 (full-screen-rectangle %background-color))
               (make <sprite>
                 #:texture chickadee-image
                 #:position (vec2 (/ %game-width 2.0)
                                  (/ %game-height 2.0))
                 #:origin (vec2 8.0 8.0)
                 #:scale (vec2 4.0 4.0))
               (make <label>
                 #:rank 1
                 #:position (vec2 (/ %game-width 2.0) 70.5)
                 #:font monogram-font
                 #:color %text-color
                 #:align 'center
                 #:vertical-align 'center
                 #:text "Made with Chickadee")
               (make <label>
                 #:rank 1
                 #:position (vec2 (/ %game-width 2.0) 50.5)
                 #:font monogram-font
                 #:color %text-color
                 #:align 'center
                 #:vertical-align 'center
                 #:text "https://dthompson.us/projects/chickadee.html"))
    (run-script scene
      (unless (getenv "SKIP_SPLASH")
        (let ((duration 0.25))
          (fade-in scene duration)
          (sleep duration)
          (fade-out scene duration)))
      (switch-to-game-scene mode))))

(define-method (switch-to-game-scene (mode <splash-mode>))
  (replace-scene (car (all-regions))
                 (make-game-scene)))

(define (launch-game)
  (set! *random-state* (random-state-from-platform))
  (run-catbird
   (lambda ()
     (let ((region (create-full-region))
           (scene (make <scene>
                    #:name 'splash
                    #:major-mode (make <splash-mode>))))
       (take-controller-focus 0 region)
       (replace-scene region scene)
       (set! (camera region)
             (make <camera-2d>
               #:width %game-width
               #:height %game-height))))
   #:width %window-width
   #:height %window-height
   #:title "Apple Town Fair"))
