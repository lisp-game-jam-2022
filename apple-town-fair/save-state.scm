;;; Copyright © 2022 David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.
(define-module (apple-town-fair save-state)
  #:use-module (apple-town-fair common)
  #:use-module (catbird config)
  #:use-module (oop goops)
  #:export (<save-state>
            %day-end
            %day-start
            %money-start
            %time-sleep
            %time-wake
            flags
            inventory
            money
            story-points))

(define %time-wake 6)
(define %time-sleep 22)
(define %day-start 0)
(define %day-end 2)
(define %money-start 20.0)

(define-root-class <save-state> ()
  (story-points #:accessor story-points #:init-keyword #:story-points
                #:init-value 0)
  (day #:accessor day #:init-keyword #:day #:init-value %day-start)
  (time #:accessor time #:init-keyword #:time #:init-value %time-wake)
  (money #:accessor money #:init-keyword #:money #:init-value %money-start)
  (inventory #:accessor inventory #:init-keyword #:inventory #:init-value '())
  (flags #:accessor flags #:init-keyword #:flags #:init-value '()))
