;;; Copyright © 2022 David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.
(define-module (apple-town-fair common)
  #:use-module (apple-town-fair config)
  #:use-module (chickadee)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee graphics path)
  #:use-module (chickadee math vector)
  #:use-module (chickadee scripting)
  #:use-module (oop goops)
  #:use-module (catbird scene)
  #:use-module (catbird node)
  #:use-module (catbird node-2d)
  #:use-module (ice-9 format)
  #:export (day
            day->string
            fade-in
            fade-out
            full-screen-rectangle
            refresh
            steps
            time
            time->string))

(define-accessor day)
(define-accessor time)
(define-generic refresh)

(define (day->string n)
  (cond
   ((= n 0) "Friday")
   ((= n 1) "Saturday")
   ((= n 2) "Sunday")
   (else "??????")))

(define (time->string n)
  (let ((n* (modulo n 12)))
    (format #f "~2,'0d:00 ~a"
            (if (= n* 0) 12 n*)
            (if (> n 11) "PM" "AM"))))


(define (steps n)
  (* n (current-timestep)))

(define (full-screen-rectangle color)
  (with-style ((fill-color color))
    (fill
     (rectangle (vec2 0.0 0.0) %game-width %game-height))))

(define (black-alpha alpha)
  (make-color 0.0 0.0 0.0 alpha))

(define (fade scene start-alpha end-alpha duration)
  (let ((bg (make <canvas>
              #:rank 999
              #:painter (full-screen-rectangle
                         (black-alpha start-alpha)))))
    (attach-to scene bg)
    (tween duration start-alpha end-alpha
           (lambda (a)
             (set! (painter bg)
                   (full-screen-rectangle
                    (black-alpha a)))))
    (detach bg)))

(define-method (fade-in (scene <scene>) duration)
  (fade scene 1.0 0.0 duration))

(define-method (fade-out (scene <scene>) duration)
  (fade scene 0.0 1.0 duration))
