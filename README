-*- org -*-

#+TITLE Lisp Game Jam 2022

* Idea

A Boku no Natsuyasumi-like about a long weekend in the Massachusetts
countryside in October, culminating in a town fair.

* Gameplay

The player traverses several different location and interacts with the
people/objects in them.  The player wakes up in the early morning
(6am) and goes to bed in the late evening (9pm.)  Most actions advance
the game by 1 hour, some will take more.  Certain actions are only
available at certain times or if certain other conditions are met, so
doing *everything* in the game requires replays and being careful
about order of operations.  However, there is no failure state.  The
game can just be enjoyed as an experience.  The exact ending will vary
based on how many story points the player earns for accomplishing
certain tasks, but every ending is a good ending.

There are items that can be bought in the game.  The player has a
fixed amount of money and cannot buy everything.  What they buy
influences later events and possible story points.

The game takes place from Friday-Sunday in October.  The exact days
will not be specified.  The game concludes at 9pm on Sunday.

Not everything is realistic.  There will be some supernatural events
for fun, such as a ghost encounter.

Boku no Natsuyasumi has several references to WWII.  This game should
have some references to the genocide of Native Americans by European
settlers.  Fall in New England is wonderful, but our cultural
traditions of apple pies and pumpkin carving are built on top of land
stolen from natives.

* Locations and what's in them

** Home
- Where the player starts and ends each day.
- Animals:
  - House cat
- Action: Bake
  - Condition: when player has apples
  - Condition: Only on Saturday
  - Player gets choices about how to make the pie:
    - Which apples to use (if they bought more than one type)
    - How much cinnamon
    - How much nutmeg
    - How much flour to thicken filling
- Action: Carve Pumpkin
  - Condition: when player has Jack o' Lantern pumpkin
- Action: Go to bed
  - Condition: Anytime after 7pm.
- Action: Watch TV
  - Add some fun slice of life informational nuggets here that change
    a few times per day.
  - One of the TV bits should be a baking show that explains how to
    bake a good pie.
  - Another TV bit should give some native american history
  - At least one of the TV events should unlock something cool
    elsewhere.
- Action: Inspect garden
  - The garden is mostly done for the year, but there are carrots that
    can be picked.  On Saturday morning, there has been a frost.
    Picking the carrots that day yields frost-sweetened carrots
    instead of the regular carrots of Friday.  On Sunday, if they
    aren't picked, the player will find that the groundhog took them
    all.

** Trail by the river
- This trail offers a break from the other, populated locations, and
  is a necessary step to get to the orchard.
- Add some interactable elements for classic New England forest finds:
  - old stone walls
  - types of trees
  - ferns
  - wildlife
- Action: Forest Bathe
  - If player has learned about pumpkin contest and has Jack o'
    Lantern, they will become inspired, improving their carving and
    increasing their rank in the contest.
  - Otherwise, just share some interesting observation about the New
    England forest with the player.
- Event: Deer encounter
  - If the player has carrots and shows up between 6am-9am on
    Saturday, the deer will approach the player.
  - If the player has frost sweetened carrots, there will be very
    pleased and will lead the player off the trail to a small defunct,
    overgrown orchard with an heirloom apple tree of unknown variety.

** Farm stand
- People/Animals:
  - Farm stand cashier
- Store is open from 9am-5pm each day
  - Buy pumpkins (Sugar or Jack o' Lantern)

** Orchard
- People:
  - Orchard store cashier
  - Dog
- Store is open from 10am-4pm each day
  - Buy 1/2 peck of apples (Cortland, Macoun, Honeycrisp)
  - Buy cider/cider donuts??
- Event: Ghosts in the orchard
  - Condition: Friday night on or after 8pm
  - Just a fun spooky event.  Nothing bad happens.

** Town common
- Interact: Stone marker memorializing when the town was burned down,
  righfully so, by native Americans.
- People:
  - Person on bench (Friday and Saturday)
- Action: Learn about upcoming town fair and associated contests.
  - Condition: On Friday or Saturday
- Event: Sunday, 10am-4pm, Town Fair
  - Apple pie contest
    - Player gets first place if:
      - They found and used the heirloom apples
      - They used 1tsp of cinnamon
      - They used 1/4tsp of nutmeg
      - They added flour to thicken the filling
  - Quilt raffle
  - Action: Buy raffle tickets
    - 0-3 tickets can be purchased.  If player has enough money left
      to buy all 3, then they will win the raffle.
- Event: Sunday, 5pm-7pm, Pumpkin Walk
  - Pumpkin carving contest
    - Players gets 1st place if:
      - They carved Jack o' Lantern (not Sugar Pumpkin)
      - They got inspired during forest bathing
    - Player gets 2nd place if:
      - They carved Jack o' Latern
      - They did not get inspired forest bathing
    - Player gets 3rd place otherwise
